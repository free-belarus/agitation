Люди не выходят бастовать не потому, что они не поддерживают. Поддерживают, просто есть страх и барьеры у многих.

Когда мы ругаем за «трусость» — это не помогает. Человек всегда защищается, нельзя заставить принять решение. Более того если человек его неосознанно, скажем просто за компанию с коллегами, принял, так его «палкой» переубедит условный «идеолог» (КГБ).
Надо позитивно призывать, человек сам должен понять. Когда человек сам понял — его решение уже осознанно, и его не сломить. 

Надо всем понять, что перемены уже 100% произойдут. Всё, режим не устоит, АГЛ не обладает легитимностью, народ не выдал мандат и говорят об этом все слои и представители всех профессий (даже силовых). Вопрос только как быстро режим сменится и насколько болезненно для нас.

💸 Сейчас чем дольше узурпатор у власти, тем больше жертв, и тем хуже с экономикой. Чем глубже кризис, тем сложней потом будет из него выбираться. Если держит то, что пока платят зарплату, то если АГЛ уйдет сейчас — на работе восстановят — не проблема, а вот если задержится — платить будет нечем довольно долго.

Лучше посмотреть какой на данный момент расклад:

1. Лукашенко осталось на пару месяцев или год (зависит от нас)
2. Конкретный раскол элит — чиновники выжидают, ничего не делают, есть высокопоставленные кто ушел, явно есть раскол в силовых («перетрахивания» — видимо сомневается в лояльности). Всё держится на страхе.
3. Тихановская наладила связи и поддержку стран Европы (которые помогут с реформами финансово), есть КС с тысячами умных деятельных людей с опытом управления, есть платформа Перамен с кучей IT-идей и реформ от самых активных молодых, НАУ (национальное антикризисное управление) — опытные люди с видением, готовы обеспечить стабильность страны сразу же после ухода Луки. То есть уже настал момент, когда есть люди, готовые взять ответственность временно, до проведения новых выборов. Как только Лука уйдет — сразу же и Россия поддержит финансово, т.к. она заинтересована в том, чтобы был баланс отношений с ними и с ЕС.
4. Тихановская гарантировала восстановление всех кто был уволен за гражданскую позицию в наше время беззакония.
5. На случай увольнения есть программы переобучения и помощью с трудоустройством (ByChange), фонд который со 100% вероятностью поможет финансово (BySol), фонд который поможет сразу же с едой (INeedHelpBY) еще Скорая Взаимопомощь от Честных Людей. Понятно, что самые бойкие люди скажут: «да мне не надо помогать!». Но ведь бывает по-разному, а если есть дети, то и вообще от помощи отказываться странно. Тем более что помогают свои земляки и земляки кто сейчас за межой. То есть за тот месяц, что мы будем бастовать семья не останется без еды, так еще и можно подучиться!


# План действий по забастовкам

1. выход из гос профсоюза, вход в независимый профсоюз — уволили за это? (по закону не могут — статья для директора) но «иногда не до законов», так что см. пункт 5. Не уволили? — отлично, профсоюз теперь может и помочь ещё защитить от увольнения (это уже работало на МТЗ и работает у шахтеров)
2. Выход всех на один день бьёт сильно по АГЛ психологически. Это уже может всё решить. 
3. Неделя — будет сложно (будут пытаться давить), но две — всё режим разламывается. Месяц — смерть режима.


# Надо продержаться максимум месяц

Даже если у вас нет никаких запасов вообще, за месяц не закончатся деньги ни у каких фондов (они только будут пополняться ещё потому что сотни тысяч людей в том числе из диаспор максимально заряжены помогать с переменами). За месяц можно получить еще и улучшение квалификации или что-то подучить. Ну или даже просто отдыхать: костер пожечь, рыбу половить, дрова поколоть. Ну да, будет непросто, будут пугать, кого-то задержат на марше или на проходной, но мы же боремся со злом, любой фильм покажет, что это непросто.

Вы скажете, а что если месяц и он не уйдет? Такого еще не видела история. Но, вдруг? Значит ещё месяц. Два месяца — опять же фонды будут абсолютно готовы.

Так что давайте просто это усвоим. И решение дастся каждому без каких-либо сомнений.
