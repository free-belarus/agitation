# ПОМНИ ЭТИ ИМЕНА

## Александр Тарайковский

г. Минск 

Погиб 10 августа от открытой раны грудной клетки (на видео видно — силовики стреляли в безоружного в упор). Уголовное дело не возбуждено.


## Александр Вихор

г. Гомель

Скончался 12 августа — ему, с 9 по 12 августа находившемуся «на сутках» в СИЗО, не оказывалась требуемая из-за побоев медицинская помощь. 

Уголовное дело не возбуждено.


## Геннадий Шутов

г.Брест.

Скончался 19 августа от огнестрельной раны в голову, полученной при задержании на протестах 11 августа. 

Уголовное дело не возбуждено.


## Роман Бондаренко

г. Минск. 

Похищен 11 ноября неизвестными во дворе своего дома, по пути в РОВД Центрального района г. Минска или в самом РОВД избит неизвестными до смерти, скончался 12 ноября в больнице. 

Уголовное дело не возбуждено.

Многие случаи смерти нужно дорасследовать, т.к. выводы «правоохранительных» органов вызывают обоснованные сомнения. Например:


## Константин Шишмаков

Член избирательной комиссии, отказавшийся подписывать ложный протокол. Пропал 15 августа, найден мертвым 19 августа.


## Никита Кривцов

Активист протеста, пропал без вести 12 августа, найден повешенным 22 августа.


## Александр Будницкий

Пропал 11 августа в районе протестов, найден мертвым в районе универмага «Рига» г. Минска только 31 августа.


## Денис Кузнецов

Попал на сутки в «Окрестина» 29 сентября случайно, так как шел на работу. Избит сотрудниками изолятора (об этом успел сообщить медикам), скончался в больнице 3 октября. Версия властей — упал со второго яруса кровати.

---

С 9 августа более 32 тысяч беларусов были задержаны и прошли тюрьмы за протесты против насилия и фальсификаций!

По многочисленным фактам избиений и пыток не заведено ни одного уголовного дела!
Преступники покрывают преступников!

Разве в такой Беларуси мы хотим жить? Я — нет! А ты?

---

# Я ТВОЙ СОСЕД

С кем-то — по лестничной площадке, с кем-то — по рабочему месту. С кем-то я езжу  в автобусе, с кем-то — стою в очереди в магазине.

И все мы знаем, что произошло этим летом... Что происходит до сих пор.

Хочу признаться тебе, мой сосед. Я боюсь.

Боюсь, но не могу молчать, мое терпение переполнено.

Я не бастую, потому что боюсь потерять работу и лишить свою семью средств к существованию.

Я не выхожу на митинги против насилия, потому что боюсь быть избитым омоновцами и попасть в тюрьму.

Но разве это правильно, разве это нормально — бояться в своей стране только потому, что не можешь терпеть беззаконие, насилие и ложь?

Согласись, мой сосед, так не должно быть! И этой листовкой я делаю то, на что у меня хватает смелости.

Хотя бы это.

Я не призываю тебя к тому, чего не делаю сам. Я призываю тебя ПОМНИТЬ!

- Помнить, что «власти» сфальсифицировали выборы.
- Помнить, что тысячи беларусов уволены, сидели и продолжают сидеть в тюрьмах.
- Помнить, что по случаям насилия, пыток и убийств не заведено НИ ОДНОГО уголовного дела.

А если ты можешь делать больше меня, если у тебя смелости больше моего — делай!

Но хотя бы помни, не забывай.

Время перемен придет, а может быть…

Может быть, мой дорогой друг, уже пришло.
