# Диктатор и домохозяйка: история одного протеста

*Примерно 10—15 минут чтения.*

Просторный церемониальный зал президентского дворца наполняла музыка. Инаугурация была в самом разгаре, но на сей раз проходила не совсем привычно. Практически полное отсутствие иностранных гостей и журналистов, полусекретность, ритуал лишь в кругу «своих» — всё это было необычно и накладывало тревожный отпечаток на мероприятие. В помещении царила нарочитая атмосфера торжественности, но ни сам новоиспечённый президент, ни малочисленная публика не выказывали восторга под стать событию. Несмотря на убедительную победу на выборах, «слуга народа» явно продолжал переживать за своё кресло: за пределами дворца его налаженная административная машина с треском проигрывала скромной женщине, подавшейся в политику вслед за своим мужем. Тем временем в столице росли баррикады, а столкновения толпы с жандармами принимали стихийный характер. Улицы кричали: «Хватайте змей!»

Может показаться, что это вольный пересказ недавних белорусских событий, но на самом деле эта история произошла треть века назад на другом конце планеты. Как Филиппинам во главе с простой домохозяйкой удалось избавиться от могущественного диктатора и насколько история любит повторяться?

## Фердинанд Маркос 


Малыш Фердинанд родился осенью 1917 года и стал долгожданным первенцем в семье Маркос. Семья проживала на севере Филиппин, в традиционно аграрном, консервативном регионе Илокос. Мать — дочь помещика и учительница, отец — адвокат, с головой окунувшийся в националистическое движение после рождения своего старшего сына. С раннего детства Фердинанд проникся простой атмосферой небольших илоканских местечек, а от своих родителей он смог унаследовать природную харизму и расчётливость в делах. Всё это, безусловно, сильно повлияло на личность будущего диктатора: на всём протяжении своей политической карьеры Маркос умудрялся публично купаться в роскоши, не теряя своего незамысловатого, «понятного» простому избирателю шарма.

Во время обучения в столичном университете Маркос становится одним из студенческих лидеров. Красноречие и политическое чутьё сопутствуют его успехам повсюду: будущий диктатор возглавляет студенческие демонстрации, умело топит конкурентов во время дебатов, зачитывает страстные речи о свободе и борьбе перед огромной аудиторией. Но планомерное восшествие по карьерной лестнице задерживает война. В декабре 1941 года японская армия вторгается на Филиппины, а Маркос в звании лейтенанта отправляется на фронт.

![https://static.baza.io/6d539bb6fa723ab8a8534511124a07e9__900x.jpg](Фердинанд Маркос в юные годы. Фото: rappler.com)

Здесь история Фердинанда Маркоса начинает сильнее всего полниться слухами и мифами: одни историки утверждают, что Маркос прошёл японские лагеря, затем примкнул к партизанам и возглавил многотысячный отряд, став чуть ли не самым отличившимся филиппинцем за всю войну. Другие говорят, что выйти из лагерей живым Фердинанд смог лишь благодаря покровительству своего отца, который якобы работал пропагандистом у японцев. Так или иначе, официальная маркосовская история приписала ему ни больше ни меньше тридцать три боевые награды, включая высшую награду США — Медаль Почёта.


## Диктаторские замашки

После войны Маркос снова возвращается в политику, но на сей раз не как горячий студент, а как настоящий ветеран национально-освободительной войны. Майор Маркос сразу предстаёт прагматичным государственником и не стесняется менять партии в зависимости от политической конъюнктуры. На свой первый президентский срок Маркос выдвигается в 1965 году и сразу берётся за масштабные национальные проекты: инфраструктурные объекты, электрификация, памятники архитектуры в стиле брутализма — в «стиле тиранов».


В 1969 году Маркос победил и на своих вторых выборах. Если в первый раз он смог отвоевать президентское кресло у своего оппонента с минимальным разрывом, то теперь «слуга народа» оставил ближайшего из одиннадцати своих противников позади на двадцать процентов.

![Фердинанд Маркос. Фото: Getty Images](https://static.baza.io/c3898fbfa0e571fcf51f6fd5bd7635bb__900x.jpg)

Пожалуй, самым экстравагантным эпизодом предвыборной кампании Маркоса были съёмки фильма Maharlika. Лента снималась на деньги президента о нём самом: бюджет позволил пригласить даже голливудских кинозвёзд того времени. Роль любовницы Фердинанда сыграла Дови Бимс, которая и в реальной жизни сумела стать любовницей Маркоса. После завершения съёмок Дови внезапно опубликовала интимные аудиозаписи их совместного времяпрепровождения, дабы защититься от угроз, поступавших со стороны Маркоса в её адрес. В своей книге The Marcos Dynasty историк Стерлинг Сигрейв приводит интересный эпизод, связанный с этими записями:

>Протестующие студенты Университета Филиппин захватили радиостанцию кампуса и начали передавать запись на повторе; вскоре вся страна слушала в изумлении, как президент Маркос умолял Дови Бимс заняться с ним оральным сексом. [После этого инцидента] больше недели хриплые президентские постановления раздавались из университетских громкоговорителей.


В сентябре 1972 года в Филиппинах из-за участившихся случаев протеста ввели военное положение — под его прикрытием Маркос сумел отменить действие конституции и рассадить по тюрьмам всех своих политических оппонентов — тогда в числе заключённых оказался и оппозиционер-националист Ниной Акино. Военное положение под флагом восстановления экономического равенства и прав человека обернулось настоящей войной против сильных на тот момент националистов и либералов. В ходе политических преследований тысячи человек были расстреляны на улицах и в тюрьмах и прошли через пытки.

![Маркос вводит военное положение. Фото: wikimedia.org](https://static.baza.io/0b82d1b3911f33c3498cf690ef0c5bf3__900x.jpg)

На фоне многолетней резни президентские стройки продолжали создавать видимость прогресса. Памятники правлению Маркоса зачастую превращались в неподъёмные долгострои и, пожалуй, самым известным из них был «Народный парк в небесах».

По изначальной задумке парк должен был стать пространством для народных собраний и публичного отдыха, но, несмотря на своё демократичное название, Народный парк с первого дня стал режимным объектом, где Маркос начал возводить резиденцию специально для приёма американского президента Р. Рейгана. Строительство объекта велось день и ночь в ускоренном темпе, местных фермеров выгнали из собственных владений, а саму площадку оцепила вооружённая охрана. Но когда Белый дом отменил визит Рейгана в Филиппины, проект внезапно исчерпал свой бюджет и заглох. Иронично то, что сегодня полуразрушенный и разрисованный граффити «Народный парк» действительно стал местом пикников и гуляний для простых филиппинцев.

![«Народный парк в небесах». Фото: media.philstar.com](https://static.baza.io/5b11997733df218a11386dc04c0574df__900x.jpg)

При поддержке армии и полиции Фердинанд Маркос управлял страной до начала 80-х годов, жёстко подавляя любое недовольство его властью, пока затихшее сопротивление оппозиции не вспыхнуло с новой силой из-за убийства давнего политического противника властей, Ниноя Акино.


## Ниной и Кори

Ниной Акино в течение семи лет продолжал легальную политическую борьбу прямо из тюремной камеры. В 1977 году военным трибуналом Ниной был приговорён к смерти, но в связи с бурной реакцией внутри страны и за её пределами политику была дана отсрочка. Во многом пережить одиночное заключение, будучи фактически приговорённым к смерти, Ниной смог благодаря своей жене, Корасон Акино.

Кори, как стали называть её филиппинцы, проводила кампании в защиту своего мужа и координировала его политические акции. С её помощью Ниной даже создал националистическую партию LABAN (что в переводе с тагальского означает «борьба» или «оппозиция»), принявшую участие в парламентских выборах 1978 года. Но помимо этого биография Кори была обычной для миллионов женщин по всему миру: учительница математики, мать, «заурядная домохозяйка», как говорила она о себе годы спустя. Участвовать в большой политике Кори не готовилась и не хотела, но вышло так, что она коснулась её семьи напрямую.

![Ниной Акино. Фото: wikimedia.org](https://static.baza.io/092b80224c57eb42d4357b33c747c468__900x.jpg)

Весной 1980 года в своей камере Ниной Акино пережил два сердечных приступа подряд и вместе с женой и детьми был выслан в США. Спустя некоторое время политическая обстановка на родине снова начала накаляться, поэтому в 1983-м он принял решение вернуться на Филиппины. Несмотря на недвусмысленные намёки об отложенном смертном приговоре со стороны властей, приезд Ниноя освещался в прессе как второе рождение оппозиции в стране. Во время полёта Ниной дал своё последнее интервью, в котором он сказал: 

>**Если они считают, что я виновен, то я готов к тому, что меня пристрелят.**

После того как самолет остановился перед терминалом, вооружённые винтовками и пистолетами солдаты наполнили салон. Ниноя вывели на полосу, приказав остальным пассажирам оставаться внутри. Спустя несколько секунд прозвучали выстрелы, а когда пассажирам разрешили покинуть самолёт, тело Ниноя уже было погружено в полицейский грузовик.


![Тело Ниноя кладут в полицейский грузовик. Фото: manilamail.us](https://static.baza.io/4a0f3c2753f0da0469b71d50bc46c09b__900x.jpg)


## Революция с женским лицом

События в аэропорту положили начало конца диктатуры Фердинанда Маркоса. Ставшая вдовой Кори Акино вернулась на Филиппины сразу после своего покойного мужа не только для того, чтобы похоронить его, но и с целью продолжить его дело. За последующие три года Корасон смогла вновь объединить филиппинскую оппозицию и, более того, возглавить её. Неприметная женщина, до сего момента находившаяся в тени мужа, Кори принимала свою роль без лишнего восторга.


>Я знаю свой предел, и мне не нравится политика. Я занялась ей только из-за своего мужа.\
\
— Корасон Акино

![Кори Акино. Фото: s.yimg.com](https://static.baza.io/ebda1ba34dc04e1498ed9e747f1c1083__900x.jpg)

С каждым новым выступлением её речи собирали огромные аудитории во всех уголках страны, а энергичные кампании Кори объединяли филиппинцев общей идеей — идеей честных и свободных выборов. Кори смогла превзойти своего мужа, хотя сама она всегда отводила себе лишь роль «продолжателя дела».

В 1986 году на внеочередных президентских выборах Кори вышла один на один с Фердинандом. После подсчёта голосов официальная избирательная комиссия признала Маркоса победителем с 53% голосов, но аккредитованная наблюдательная организация NAMFREL выявила огромный список нарушений и заключила победу Акино — с 52%. Этого стало достаточно для начала протестов по всей стране.

Столкновения протестующих и специальных подразделений полиции вылились в события, позже названные Революцией народной власти, или просто Жёлтой революцией, — по цвету ленточек, которыми обозначали себя протестующие. Это была отсылка к песне о бывшем заключённом, который возвращается домой к своей любимой.

![Жёлтая революция. Фото: thoughtco.com](https://static.baza.io/6103a960676ede61d27d1cd78029ee4c__900x.png)

Инаугурация Маркоса была проведена в президентском дворце, оцепленном военными, в окружении лишь своих генералов, жены и ближайших сторонников. За высокими стенами дворца протестующие кричали: «Ловите змей!» Акино, в свою очередь, принесла присягу в клубе «Филипино», у крыльца которого толпа громко распевала патриотические песни. Позже Маркос сбежит из страны на американском военном самолёте, а Корасон будет править с 1986 по 1992 год.


>У меня нет формулы, по которой можно изгнать диктатора или построить демократию. Всё, что я могу предложить, — это забыть о себе и думать о своём народе. Всё, что ни происходит, — свершается народом.\
\
— Корасон Акино


![Кори приносит присягу. Фото: wikimedia.org](https://static.baza.io/8dee0b3853d284f15973f29f8c560743__900x.jpg)

---

## Источники

### Статьи

1. David McKittrick, Cory Aquino: President of the Philippines who brought democracy to the islands, Independent, 4 августа 2009
2. Atty. Jose Ferdinand M. Rojas II, The last journey of Ninoy, Business Mirror, 24 августа 2020
3. Ken Kashiwahara, Aquino’s Final Journey, The New York Times, 16 октября 1983
4. Rodel Rodis, Remembering the First Quarter Storm, Philippine Daily Inquirer, 30 января 2015
5. Barbara Mae Naredo Dacanay, The 7 deadly protests of the First Quarter Storm, ABS-CBN News and Public Affairs, 24 февраля 2020
6. Rachel A.G. Reyes, 3,257: Fact checking the Marcos killings, 1975-1985, The Manila Times, 12 апреля 2016
7. Ronald U. Mendoza, Ferdinand Marcos’ economic disaster, 26 февраля 2016
8. Janet Cawley, Marcos’ Mountain Palace is the House That Arrogance Built, Chicago Tribune, 30 марта 1986

### Прочее

1. Jim Laurie, Full Airplane interview from August 21 1983 with Ninoy Aquino, YouTube, 22 июля 2014\
\
@[youtube](eBD4vJS0dPk)
2. Amnesty International, Report of an AI Mission to the Republic of the Philippines 1975, 1 сентября 1976, Index number: ASA 35/019/1977
3. National Bureau of Economic Research, Developing Country Debt and Economic Performance, Volume 3: Country Studies — Indonesia, Korea, Philippines, Turkey, 1989
4. Sterling Seagrave, The Marcos Dynasty


---

Автор: 
Никита Барышников
[Оригинал: baza.io](https://baza.io/posts/8a532915-7cb4-4d73-9b06-a70aca3f624c)
