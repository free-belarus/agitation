# 11 мифов госпропаганды

1. Чу! Слышится лязг натовских гусениц… (хотя за неделю до выборов, всего полгода назад, главным врагом была Россия)
2. БЧБ — это экстрим (российский триколор и пр. не считаются), а Закат над Болотом — символ терроризма.
3. Лидеры сбежали (почему им пришлось уехать и про тех, кого посадили ни слова)
4. Мифические кукловоды: кто они и где? Я вот уже всех кого могла спросить спросила (а у меня есть у кого поинтересоваться) — к сожалению, кукловодов нет. 
5. Оппозиция деструктивна хочет все развалить и лишила людей чемпионата по хоккею (про свою никчемность тоже ни слова. И уж конечно ни слова про коррупционные схемы «семьи» и прикорытников и вывод денех)
6. Протестунов мало, это секта (тоже манипулятивный прием, так как люди склонны примыкать к большинству, поэтому они упорно твердят, что нас мало) 
7. Дискредитация протестующих (наркоманы, проститутки)
8. Цветная революция не удалась (опять, что революция — это экспорт, а не внутренние проблемы и «не удалась», чтобы снизить моральный дух)
9. Отрицание/замалчивание насилия силовиков, мол результатов экспертизы нет, все это хайп и надуманное. 
10. Ну и да… выборы были чэсныя
11. Власть дается по праву рождения, женщина у власти не может быть и прочая чушь про сакральность власти
