---
title: Работа с исходниками
description: Пояснения о том как работать с исходными материалами этого репозитория
---

# Работа с исходниками

- Исходники в векторном формате — `*.ai` — лежат в корне папки. Используем актуальную версию [Adobe Illustrator CC 2021](https://www.adobe.com/ru/products/illustrator.html). 
- [Шрифты от Apple](https://developer.apple.com/fonts/), если не получается их установить на Windows, то попробуйте [вот этот архив](https://www.dropbox.com/s/x2vuy052m3xggjw/Apple-Fonts-NY-SF.zip?dl=0).
- `in` — используемые материалы скачанные из интернета
- в корне папки лежит файл `README.md` с описанием проекта

Если вам нужен исходник, но что-то не получается — [свяжитесь с нами](https://free-belarus.info/contribute/feedback.md)
